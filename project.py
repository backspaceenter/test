
from sqlalchemy import create_engine, MetaData, Column, Integer, String, Float
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import sqlalchemy

Base = declarative_base()


class CurrWeather(Base):
    __tablename__ = 'weather'
    id = Column(Integer, primary_key=True)
    city_name = Column(String(50))
    weather_description = Column(String(50))
    feels_like = Column(Float)

    def __init__(self, city_name, weather_description, feels_like):
        self.city_name = city_name,
        self.weather_description = weather_description,
        self.feels_like = feels_like

    def __repr__(self):
        return "<CurrWeather('%s', '%s', '%d')>" % (self.city_name, self.weather_description, self.feels_like)


engine = create_engine('mysql://root:pass@127.0.0.1:3306/', echo=False)
engine.execute("""create database if not exists weather""")
engine = create_engine('mysql://root:pass@127.0.0.1:3306/weather?use_unicode=1&charset=utf8', echo=False)
if not sqlalchemy.inspect(engine).has_table('weather'):
    CurrWeather.metadata.create_all(engine)
else:
    CurrWeather.metadata.drop_all(engine)
    CurrWeather.metadata.create_all(engine)
engine.execute("""ALTER TABLE weather CONVERT TO CHARACTER SET utf8""")


Session = sessionmaker(bind=engine)
session = Session()


session.add_all(
    [
        CurrWeather('Moscow', 'something', 276.28),
        CurrWeather('New York', 'good', 288.7),
        CurrWeather('London', 'bad', 279.63),
        CurrWeather('Vienna', 'Not good', 281.21)

    ]
)

for instance in session.query(CurrWeather).order_by(CurrWeather.id):
    print(instance.city_name, instance.weather_description)


session.commit()
session.close()
